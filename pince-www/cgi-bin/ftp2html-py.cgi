#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import mimetypes

base_url = "http://pince.balsamine.be/"

print """\
Content-type: text/html

    <!DOCTYPE HTML>
    <html>
        <head>
	    <meta charset="utf-8">
            <title>La Balsamine</title>
        <style>
        @font-face {
            font-family: "Ume Gothic";
            src: url("http://www.balsamine.be/Ume-P-Gothic-latin.ttf") format("truetype");
        }
        body {
            font-family: "Ume Gothic", "Arial", Sans;
            font-size: 12px;
            line-height: 16px;
            width: 100%;
            height: 100%;
            letter-spacing: 1px;
            margin: 1em;
            margin-top: 5px;
            width: 74%;
            overflow: hidden;
            color: #002D36;
        }
        div#pince {
            width: 100%;
            height: 100%;
            position: absolute;
            font-size: 10px;
            line-height: 13px;
        }
        div#news {
            z-index: 101; 
            background-color: white; 
            width: 248px; 
            position: absolute;
            padding: 10px;
        }
        div#news dt {
            float: left;
            margin-right: 8px;
            color: red;
        }
        div#sticky {
            z-index:1000 !important;
            font-size: 10px;
            line-height: 13px;
            margin-bottom: 0.5em;
        }
        div.sticky {
            display: inline;
        }
        div.postit {
            display: none;
            width: 248px;
            position: absolute;
            background-color: white;
            padding: 1em;
            z-index: 1000 !important;
            border: 1px solid #002D36;
        }
        div.sticky:hover div.postit {
            display: block;
        }
        a {
            text-decoration: none;
            padding: 2px;
            border: 1px solid #002D36;
            color: inherit;
        }
        a:visited {
            border: 1px solid darkgray;
        }
        a:hover {
            border: 1px solid black;
            color: black;
        }
        div.news {
            z-index: 200;
            border: 1px solid #002D36;
        padding: 10px;
        }
        img {
            vertical-align: top;    
            max-width: 250px;
            position: absolute;
            top: 0px;
        }
        p {
            max-width: 50em;
        }
        sup {
            font-size: 8px;
            line-height: 0;
            vertical-align: 0px;
        }
        iframe {
            width: 100%;
            height: 100%;
            z-index: -10;
            position: fixed;
            top: 40px;
            bottom: 0;
            left: 0;
            right: 0;
            border: none;
        }
        iframe:hover {
            z-index:1000;
        }
        </style>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            intro = true;
            if (intro == true) {
                $("div#pince").css("z-index", "900");
                $("iframe").css("z-index", "-10");
                $("div#pince").click(function(){
                    $("div#pince").css("z-index", "-10");
                    $("iframe").css("z-index", "0");
                    intro = false;
                });
            }

            $("div#pince, div.news, img.img-pince, div#help, div#why").hover(
                function(){
                    z = $(this).css("z-index");
                    $("div#pince").css("z-index", "900");
                    $("iframe").css("z-index", "-10");
                    $(this).css("z-index", "900");
                }, function(){
                    $(this).css({"z-index": z});
                    $("div#pince").css("z-index", "-10");
                    $("iframe").css("z-index", "0");
                }
            );
            });
        </script>
	</head>
	<body>
        <div id="sticky">
        <div id="help" class="sticky">
            <a href="#">À propos de la pince <b style="color:black;background-color:#ffff66">balsamine</b></a>
            <div class="postit">
            <p>
                Bonjour, ici la pince de la Balsa. Je collecte des images* qui ont un lien avec la programmation à venir, un peu comme celles qui s'empilaient aux bords de certains tableaux de la Renaissance, tout juste visibles. Progressivement, d'autres viendront à se joindre, et à la fin de l'été, un nouveau paysage s'ouvrira par-dessous.
            </p>

            <p>
            *Pour y jeter un œil, glisser votre pointeur sur leurs bords, tout juste visibles; pour rejoindre le site actuel, cliquez sur la partie basse de la fenêtre.
            </p>
            </div>
        </div>

        <div class="sticky">
            <a href="http://www.balsamine.be/presse/" target="_blank">Presse et partenaires</a>
            <div class="postit">

            <p>
                <a href="http://www.balsamine.be/presse/" target="_blank">Tous les éléments individuels de l'identité visuelle de la <b style="color:black;background-color:#ffff66">balsamine</b> sont accessibles ici.</a>
            </p>
            </div>
        </div>
        </div>

    <div id="pince">
        <div class="news" style="z-index: 1000 !important; background-color: white; width: 290px; position: absolute; height: 300px; padding-bottom: 20px">
            <div style="float: left; width:140px;">
<p style="height: 200px;">
<a style="border: none;" href="http://www.balsamine.be/presse/Balsamine_programme-2011-12.pdf" target="_blank"><img style="border: 1px solid #002D36; margin-top: 10px;" src="http://balsamine.be/presse/Balsamine_programme-2011-12-cover.gif" alt="Programme 2011-2012" />
</p><p>
<a href="http://www.balsamine.be/presse/Balsamine_programme-2011-12.pdf" style="border-bottom: none;" target="_blank">Programme 2011-2012<br />(textes et photos)</a> <sup>(9Mo)</sup>
</p>
            </div>

            <div style="float: left; margin-left: 10px; width:140px;">
<p style="height: 200px;">
<a style="border: none; color:white;" href="http://www.balsamine.be/presse/Balsamine-programme-2011-12-hichem_dahes.pdf" target="_blank"><img style="border: 1px solid #002D36; margin-top: 10px;" src="http://balsamine.be/presse/Balsamine_programme-2011-12-photos.jpg" alt="Programme 2011-2012 - Partie photos" /></a>
</p><p>
<a href="http://www.balsamine.be/presse/Balsamine-programme-2011-12-hichem_dahes.pdf" style="border-bottom: none;" target="_blank">Portraits d'artistes<br />2011-2012</a> <sup>(3Mo)</sup>
</p>

            </div>
<div style="clear:both; float:none">
<p>

<a href="http://www.balsamine.be/presse/" target="_blank">Tous les éléments individuels de l'identité visuelle<br />de la <b style="color:black;background-color:#ffff66">balsamine</b> sont accessibles ici.</a>
</p>
</div>
        </div>


        <div class="news" style="z-index: 101; background-color: white; width: 250px; position: absolute; left: 15px;">
        <p>
        Le <span style="color:red">samedi 25 juin 2011</span>, l'équipe de la <b style="color:black;background-color:#ffff66">Balsamine</b> et les artistes de la saison vont parcourir Bruxelles dans un bus pour vous rencontrer et vous présenter la saison 2011-2012!
        </p>

        <p>
        Vous pouvez monter et descendre du bus aux arrêts suivants (horaires susceptibles d'être légèrement décalés en fonction du trafic):
        </p>
            <dl>
                <dt>14h00</dt><dd>Départ de la <b style="color:black;background-color:#ffff66">Balsamine</b> (venez avec nous chercher votre programme à l'imprimerie!)</dd>
                <dt>15h15-15h30</dt><dd>Place Fernand Coq</dd>

                <dt>15h45-16h15</dt><dd>Palais Royal</dd>
                <dt>16h30-16h40</dt><dd>Palais de justice</dd>
                <dt>17h45</dt><dd>Terminus à la <b style="color:black;background-color:#ffff66">Balsamine</b></dd>
                <dt>18h00</dt><dd>Drink!</dd>

            </dl>
            <p>
            Attention, le nombre de places est limité.<br />
            Pour les intéressés, 02&nbsp;735&nbsp;64&nbsp;68 ou<br />
<a href="&#114;&#101;&#115;&#101;&#114;&#118;&#097;&#116;&#105;&#111;&#110;&#064;&#098;&#097;&#108;&#115;&#097;&#109;&#105;&#110;&#101;&#046;&#098;&#101;">
&#114;&#101;&#115;&#101;&#114;&#118;&#097;&#116;&#105;&#111;&#110;&#064;<b style="color:black;background-color:#ffff66">&#098;&#097;&#108;&#115;&#097;&#109;&#105;&#110;&#101;</b>&#046;&#098;&#101;</a>.
            </p>
        </div>

"""

search_dir = "/home/balsamine/www/be.balsamine.pince/"
os.chdir(search_dir)
files = filter(os.path.isfile, os.listdir(search_dir))
files = [os.path.join(search_dir, f) for f in files] # add path to each file
files.sort(key=lambda x: os.path.getmtime(x))
files.reverse()

posx = 10
posz = 100

for item in files[0:50]:
    filename = item.rsplit("/", 1)[1]
    mimetype = mimetypes.guess_type(item)
    #print filename, mimetype[0]
    if (mimetype[0] == "image/jpeg" or mimetype[0] == "image/png" or mimetype[0] == "image/gif"):
        print "<img src='%s%s' alt='%s' title='%s' style='left: %spx; z-index: %s;'/>" % (base_url, filename, filename, filename, posx, posz)
    elif (mimetype[0] == "image/svg+xml"):
	print "<embed src='%s%s'> </embed>" % (base_url, filename)
    #elif (mimetype[0] == "text/plain"):
    #    print type(item)
    #    file = open(str(item), "r") 
    #    text = file.readlines()
    #    file.close()
    #    for line in text:
    #        print "<p>" + line + "</p>" 
    posx += 15
    posz += -1


print """
        <iframe src="http://www.balsamine.be/">
        </iframe>
	</body>
    </html>
"""
