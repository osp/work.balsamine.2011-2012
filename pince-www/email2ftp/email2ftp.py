# -*- coding: utf-8 -*-

#! /usr/bin/env python

import poplib
import email
import sys
import os.path
import smtplib
from email.mime.text import MIMEText
#import textwrap



path = sys.argv[1]

if not os.path.isdir(path):
    sys.exit()

emails=[]
file = open("emails.list", "r")
emaillist = file.readlines()
file.close()
for i in emaillist:
    emails.append(i.strip("\n"))

msgs = []

M = poplib.POP3('mail.gandi.net')
M.user('joasia@stdin.fr')
M.pass_('joasia')

numMessages = len(M.list()[1])

for i in range(numMessages):
    lines = [j for j in M.retr(i+1)[1]]
    msgs.append('\n'.join(lines))
    M.dele(i+1)

for m in msgs:
    #import pdb; pdb.set_trace();
    msg = email.message_from_string(m)
    if msg.is_multipart():
        filenames = []
        sender = msg['from'].split("<")[1].strip(">")
        for i in msg.get_payload():
            if sender in emails:
                fn = i.get_filename()
                if fn is not None:
                    ct = i.get_payload(decode=True)
                    f = open(os.path.join(path, unicode(fn)), 'w')
                    f.write(ct)
                    f.close()
                    filenames.append(fn)

    if len(filenames) is not 0:
        ourmsg = MIMEText("""La pince vous dit merci pour votre contribution!
Les fichiers suivants ont été mis en ligne:

%s

Venez le(s) voir sur http://balsamine.stdin.fr/cgi-bin/ftp2html-py.cgi
et accédez à l'ensemble des fichiers en vous rendant sur 
http://pince.balsamine.stdin.fr/.

Venez me voir dans la buanderie!

-- La pince
""" % ', '.join(filenames))
        me = 'balsamine@stdin.fr'
        you = msg['from']
        ourmsg['Subject'] = 'La pince de la Balsamine vous dit merci!'
        ourmsg['From'] = me
        ourmsg['To'] = you
        s = smtplib.SMTP('localhost')
        s.sendmail(me, [you], ourmsg.as_string())
        s.quit()

M.quit()
