#! /usr/bin/python
# -*- coding: UTF-8 -*-
from gimpfu import *

def layers2svg(bla, bli, source, target):
    image = pdb.file_psd_load(str(source), str(source))
    for layer in image.layers:
        if layer.visible:
            try:
                layer.remove_mask(0) # apply mask and removes it
            except:
                pass
            pdb.gimp_selection_layer_alpha(layer) # alpha to selection
            pdb.plug_in_sel2path(image, layer) # selection to path

    svg=""
    for path in image.vectors:
        svg+= pdb.gimp_vectors_export_to_string(image, path) # exports path to xml

    file = open(str(target), "w")
    file.write(svg)
    file.close()

register(
    "python_fu_layers2svg",
    "Converts all layers to vectors and exports as SVG",
    "Converts all layers to vectors and exports as SVG",
    "OSP (Ludivine Loiseau, Pierre Huyghebaert[idea], Stéphanie Vilayphiou [script])",
    "OSP (Ludivine Loiseau, Pierre Huyghebaert[idea], Stéphanie Vilayphiou [script])",
    "GNU GPL 3",
    "<Image>/Filters/Custom/LAYERStoSVG",
    "",
    [
       (PF_STRING, "source", "Source Image", "/home/"),
       (PF_STRING, "target", "SVG output", "/home/layers2svg.svg")
    ],
    [],
    layers2svg
)
main()
